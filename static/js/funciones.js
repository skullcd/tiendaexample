function agregar_carrito(id_producto, precio,nombre){
  objeto = $("#cantidad_"+id_producto);
  cantidad = objeto.val();
  if (cantidad != 0) {
    $("#cantidad_"+id_producto).attr("max", parseInt(objeto.attr("max")) - parseInt(cantidad));
    $("#cantidad_"+id_producto).val("0");
    total = parseInt(localStorage.getItem("Total"));
    total += parseInt(cantidad) * parseInt(precio);
    localStorage.setItem("Total", total);

    productos = localStorage.getItem("Productos");
    try {
      productos = JSON.parse(productos);
    } catch (e) {
      productos = {};
    }
    if (productos[id_producto]) {
      var producto = {id:id_producto,nombre: nombre,precio:precio ,cantidad:(productos[id_producto].cantidad + parseInt(cantidad))};
      productos[id_producto] = producto;
    }else {
      var producto = {id:id_producto,nombre: nombre, cantidad:parseInt(cantidad), precio:precio};
      productos[id_producto] = producto;
    }

    localStorage.setItem("Productos",JSON.stringify(productos));
    desplegarInfo();
  }else{
    alert("No hay productos");
  }

}

function inicializar_datos(){
  localStorage.setItem("Total", 0);
  localStorage.setItem("Productos", "");
  desplegarInfo();
}


function desplegarCarrito(){

  if ($("#resumenCarrito").css("display") == "block") {
    $("#resumenCarrito").css("display", "none");
  }else {
    desplegarInfo();
    $("#resumenCarrito").css("display", "block");
  }
}


function desplegarInfo(){
  productos = localStorage.getItem("Productos");
  total = localStorage.getItem("Total");
  data = "";
  try {
    productos = JSON.parse(productos);
    for(datos in productos){
      data+="<tr>"+
      "<th scope='row'>"+productos[datos].nombre+"</th>"+
      "<td>"+productos[datos].cantidad+"</td>"+
      "<td>"+productos[datos].precio+"</td>"+
      "</tr>";
    }
  } catch (e) {
    data = "<p>Sin datos</p>"
  }
  $("#total").html("Total: $"+total);
  $("#cuerpo_tabla").html(data);
}


function comprar(){
  productos = localStorage.getItem("Productos");
  if (productos != "") {
    $.ajax({
      url: 'realizar_compra',
      type: 'GET',
      data:{productos:productos},
      success: function(data){
        if (data.trim() == "OK") {
          alert("compra realizada");
          inicializar_datos();
        }else{
          alert("No se pudo realizar la compra");
        }
      },
    });

  }else{
    alert("el carrito esta vacio");
  }
}
