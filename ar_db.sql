-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2018 at 04:29 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ar_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_amigos`
--

CREATE TABLE `ar_server_amigos` (
  `id` int(11) NOT NULL,
  `user_emisor` varchar(11) NOT NULL,
  `user_receptor` varchar(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_amigos`
--

INSERT INTO `ar_server_amigos` (`id`, `user_emisor`, `user_receptor`, `fecha`) VALUES
(5, '258846', '238253', '2018-11-27'),
(6, '238253', '258846', '2018-11-27');

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_aula`
--

CREATE TABLE `ar_server_aula` (
  `id` int(11) NOT NULL,
  `nombre` varchar(10) NOT NULL,
  `edicio` varchar(10) NOT NULL,
  `latitud` varchar(30) NOT NULL,
  `longitud` varchar(30) NOT NULL,
  `img_url` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_aula`
--

INSERT INTO `ar_server_aula` (`id`, `nombre`, `edicio`, `latitud`, `longitud`, `img_url`) VALUES
(1, 'A1', 'A', '', '', ''),
(2, 'A2', 'A', '', '', ''),
(3, 'A3', '', '', '', ''),
(4, 'A4', '', '', '', ''),
(5, 'A5', '', '', '', ''),
(6, 'A6', '', '', '', ''),
(7, 'A7', '', '', '', ''),
(8, 'A8', '', '', '', ''),
(9, 'A9', '', '', '', ''),
(10, 'A10', '', '', '', ''),
(11, 'A11', '', '', '', ''),
(12, 'A12', '', '', '', ''),
(13, 'A13', '', '', '', ''),
(14, 'A14', '', '', '', ''),
(15, 'A15', '', '', '', ''),
(16, 'A16', '', '', '', ''),
(17, 'A17', '', '', '', ''),
(18, 'A18', '', '', '', ''),
(19, 'A19', '', '', '', ''),
(20, 'A20', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_calificacion`
--

CREATE TABLE `ar_server_calificacion` (
  `id` int(11) NOT NULL,
  `calificacion` varchar(20) NOT NULL,
  `fecha` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_campoinformativocontenido`
--

CREATE TABLE `ar_server_campoinformativocontenido` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `texto` varchar(255) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `img_url` varchar(50) NOT NULL,
  `puntoInteres_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_campoinformativolista`
--

CREATE TABLE `ar_server_campoinformativolista` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `puntoInteres_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_campoinformativolista`
--

INSERT INTO `ar_server_campoinformativolista` (`id`, `titulo`, `descripcion`, `puntoInteres_id`) VALUES
(1, 'Boletin Coliseo', 'Aqui aparece toda la informacion dentreo del boletin del coliseo', 1),
(2, 'Ofertas Trabajo', 'Ofertas para practicas profecionales', 1),
(3, 'Usuarios', 'Puntos creados por los usuarios', 1),
(4, 'Boletin Canchas', 'Aqui aparece la información relevante sobre el boletin de chanchas', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_cic_cil`
--

CREATE TABLE `ar_server_cic_cil` (
  `id` int(11) NOT NULL,
  `CIC_id` int(11) NOT NULL,
  `CIL` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_cil_cil`
--

CREATE TABLE `ar_server_cil_cil` (
  `id` int(11) NOT NULL,
  `CIL_primario` int(11) NOT NULL,
  `CIL_secundario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ar_server_cil_cil`
--

INSERT INTO `ar_server_cil_cil` (`id`, `CIL_primario`, `CIL_secundario`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_comentario`
--

CREATE TABLE `ar_server_comentario` (
  `id` int(11) NOT NULL,
  `comentario` varchar(250) NOT NULL,
  `fecha` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `estatus` varchar(20) NOT NULL,
  `tipoComentario` varchar(50) NOT NULL,
  `id_clase_docente` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_comentario`
--

INSERT INTO `ar_server_comentario` (`id`, `comentario`, `fecha`, `user_id`, `estatus`, `tipoComentario`, `id_clase_docente`) VALUES
(1, 'Esta bien cool ', '2018-12-02', 258858, 'Aprobado', 'Clase', '1'),
(2, 'Esta bien cool x2', '2018-12-02', 258858, 'Pendiente', 'Clase', '3'),
(3, 'Ggg', '2018-12-02', 258858, 'Aprobado', 'Clase', '1'),
(4, 'Esta bien cool ggg man ', '2018-12-02', 258858, 'Pendiente', 'Clase', '1'),
(5, 'Es el patron ', '2018-12-02', 258858, 'Aprobado', 'Docente', '285468'),
(6, 'La materia me gustó', '2018-12-10', 258858, 'Pendiente', 'Clase', '3');

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_datosuser`
--

CREATE TABLE `ar_server_datosuser` (
  `id` int(11) NOT NULL,
  `img_url` varchar(50) NOT NULL,
  `plan` varchar(15) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_datosuser`
--

INSERT INTO `ar_server_datosuser` (`id`, `img_url`, `plan`, `rol_id`, `user_id`) VALUES
(3, '', 'SOF11', 1, 258858),
(4, '', 'SOF11', 1, 258846),
(5, '', 'SOF11', 1, 258834),
(6, '', 'SOF11', 1, 278795),
(7, '', 'SOF11', 1, 258857),
(8, '', 'SOF11', 1, 258853),
(9, '', 'SOF11', 1, 259615),
(10, '', 'SOF11', 1, 238253),
(11, '', 'SOF11', 1, 258842),
(12, '', 'SOF11', 1, 267802),
(13, '', 'SOF11', 1, 267785),
(14, 'alex', 'SOF11', 2, 285468),
(15, 'lalo', 'SOF11', 2, 285469),
(16, 'diego', 'SOF11', 2, 285470),
(17, '', 'SOF11', 1, 258841),
(18, '', 'SOF11', 1, 235838),
(19, '', 'SOF11', 3, 666),
(20, '', 'SOF11', 1, 234643),
(21, '', 'LAT11', 1, 261732);

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_docente`
--

CREATE TABLE `ar_server_docente` (
  `id` int(11) NOT NULL,
  `cubiculo` varchar(30) NOT NULL,
  `extencion` varchar(15) NOT NULL,
  `user_id` int(11) NOT NULL,
  `informacionAdicional` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_docente`
--

INSERT INTO `ar_server_docente` (`id`, `cubiculo`, `extencion`, `user_id`, `informacionAdicional`) VALUES
(1, 'Centro Desarrollo', '59', 285468, 'No'),
(2, 'Centro Desarrollo', '59', 285469, 'No'),
(3, 'Centro Desarrollo', '59', 285470, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_documentos`
--

CREATE TABLE `ar_server_documentos` (
  `id` int(11) NOT NULL,
  `nombreDocumento` varchar(100) NOT NULL,
  `url_documento` varchar(100) NOT NULL,
  `idDocente_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ar_server_documentos`
--

INSERT INTO `ar_server_documentos` (`id`, `nombreDocumento`, `url_documento`, `idDocente_id`) VALUES
(3, 'docente.csv', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1/285468', 285468),
(4, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A2/', 285468),
(5, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1285468', 285468),
(6, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1/285468', 285468),
(7, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1/285468', 285468),
(8, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1/285468', 285468),
(9, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1/285468', 285468),
(10, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1/285468', 285468),
(11, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1', 285468),
(12, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1', 285468),
(13, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1/285468', 285468),
(14, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A1/285468', 285468),
(15, '21 A NTHE HI Obsv atendidas autor_revLL.docx', 'C:\\Users\\carlo\\Desktop\\AR_Django\\static/Documents/A4/285468', 285468);

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_horario`
--

CREATE TABLE `ar_server_horario` (
  `id` int(11) NOT NULL,
  `horaInicio` varchar(45) DEFAULT NULL,
  `horaFinal` varchar(45) DEFAULT NULL,
  `dia` varchar(20) NOT NULL,
  `aula_id` int(11) NOT NULL,
  `docente_id` int(11) NOT NULL,
  `materia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_horario`
--

INSERT INTO `ar_server_horario` (`id`, `horaInicio`, `horaFinal`, `dia`, `aula_id`, `docente_id`, `materia_id`) VALUES
(1, '07:00', '09:00', 'Lunes', 1, 1, 1),
(2, '09:00', '11:00', 'Lunes', 1, 2, 2),
(3, '11:00', '13:00', 'Lunes', 1, 3, 3),
(4, '13:00', '15:00', 'Lunes', 1, 1, 4),
(5, '15:00', '17:00', 'Lunes', 1, 2, 5),
(6, '17:00', '19:00', 'Lunes', 1, 1, 6),
(7, '19:00', '21:00', 'Lunes', 1, 2, 7),
(8, '07:00', '09:00', 'Lunes', 2, 3, 1),
(9, '07:00', '09:00', 'Martes', 1, 1, 1),
(10, '09:00', '11:00', 'Martes', 1, 2, 2),
(11, '11:00', '13:00', 'Martes', 1, 3, 3),
(12, '13:00', '15:00', 'Martes', 1, 1, 4),
(13, '15:00', '17:00', 'Martes', 1, 2, 5),
(14, '17:00', '19:00', 'Martes', 1, 3, 6),
(15, '19:00', '21:00', 'Martes', 1, 1, 7),
(16, '07:00', '09:00', 'Miercoles', 1, 1, 1),
(17, '09:00', '11:00', 'Miercoles', 1, 2, 2),
(18, '11:00', '13:00', 'Miercoles', 1, 3, 3),
(19, '13:00', '15:00', 'Miercoles', 1, 1, 4),
(20, '15:00', '17:00', 'Miercoles', 1, 2, 5),
(21, '17:00', '19:00', 'Miercoles', 1, 3, 6),
(22, '19:00', '21:00', 'Miercoles', 1, 1, 7),
(23, '07:00', '09:00', 'Jueves', 1, 1, 1),
(24, '09:00', '11:00', 'Jueves', 1, 2, 2),
(25, '11:00', '13:00', 'Jueves', 1, 3, 3),
(26, '13:00', '15:00', 'Jueves', 1, 1, 4),
(27, '15:00', '17:00', 'Jueves', 1, 2, 5),
(28, '17:00', '19:00', 'Jueves', 1, 3, 6),
(29, '19:00', '21:00', 'Jueves', 1, 1, 7),
(30, '07:00', '09:00', 'Viernes', 1, 1, 1),
(31, '09:00', '11:00', 'Viernes', 1, 2, 2),
(32, '11:00', '13:00', 'Viernes', 1, 3, 3),
(33, '13:00', '15:00', 'Viernes', 1, 1, 4),
(34, '15:00', '17:00', 'Viernes', 1, 2, 5),
(35, '17:00', '19:00', 'Viernes', 1, 3, 6),
(36, '19:00', '21:00', 'Viernes', 1, 1, 7),
(37, '07:00', '09:00', 'Sabado', 1, 1, 1),
(38, '09:00', '11:00', 'Sabado', 1, 2, 2),
(39, '11:00', '13:00', 'Sabado', 1, 3, 3),
(40, '13:00', '15:00', 'Sabado', 1, 1, 4),
(41, '15:00', '17:00', 'Sabado', 1, 2, 5),
(42, '17:00', '19:00', 'Sabado', 1, 3, 6),
(43, '19:00', '21:00', 'Sabado', 1, 1, 7),
(44, '07:00', '09:00', 'Lunes', 2, 1, 1),
(45, '09:00', '11:00', 'Lunes', 2, 2, 2),
(46, '11:00', '13:00', 'Lunes', 2, 3, 3),
(47, '13:00', '15:00', 'Lunes', 2, 1, 4),
(48, '15:00', '17:00', 'Lunes', 2, 2, 5),
(49, '17:00', '19:00', 'Lunes', 2, 3, 6),
(50, '19:00', '21:00', 'Lunes', 2, 1, 7),
(51, '07:00', '09:00', 'Martes', 2, 1, 1),
(52, '09:00', '11:00', 'Martes', 2, 2, 2),
(53, '11:00', '13:00', 'Martes', 2, 3, 3),
(54, '13:00', '15:00', 'Martes', 2, 1, 4),
(55, '15:00', '17:00', 'Martes', 2, 2, 5),
(56, '17:00', '19:00', 'Martes', 2, 3, 6),
(57, '19:00', '21:00', 'Martes', 2, 1, 7),
(58, '07:00', '09:00', 'Miercoles', 2, 1, 1),
(59, '09:00', '11:00', 'Miercoles', 2, 2, 2),
(60, '11:00', '13:00', 'Miercoles', 2, 3, 3),
(61, '13:00', '15:00', 'Miercoles', 2, 1, 4),
(62, '15:00', '17:00', 'Miercoles', 2, 2, 5),
(63, '17:00', '19:00', 'Miercoles', 2, 3, 6),
(64, '19:00', '21:00', 'Miercoles', 2, 1, 7),
(65, '07:00', '09:00', 'Jueves', 2, 1, 1),
(66, '09:00', '11:00', 'Jueves', 2, 2, 2),
(67, '11:00', '13:00', 'Jueves', 2, 3, 3),
(68, '13:00', '15:00', 'Jueves', 2, 1, 4),
(69, '15:00', '17:00', 'Jueves', 2, 2, 5),
(70, '17:00', '19:00', 'Jueves', 2, 3, 6),
(71, '19:00', '21:00', 'Jueves', 2, 1, 7),
(72, '07:00', '09:00', 'Viernes', 2, 1, 1),
(73, '09:00', '11:00', 'Viernes', 2, 2, 2),
(74, '11:00', '13:00', 'Viernes', 2, 3, 3),
(75, '13:00', '15:00', 'Viernes', 2, 1, 4),
(76, '15:00', '17:00', 'Viernes', 2, 2, 5),
(77, '17:00', '19:00', 'Viernes', 2, 3, 6),
(78, '19:00', '21:00', 'Viernes', 2, 1, 7),
(79, '07:00', '09:00', 'Sabado', 2, 1, 1),
(80, '09:00', '11:00', 'Sabado', 2, 2, 2),
(81, '11:00', '13:00', 'Sabado', 2, 3, 3),
(82, '13:00', '15:00', 'Sabado', 2, 1, 4),
(83, '15:00', '17:00', 'Sabado', 2, 2, 5),
(84, '17:00', '19:00', 'Sabado', 2, 3, 6),
(85, '19:00', '21:00', 'Sabado', 2, 1, 7),
(86, '07:00', '09:00', 'Lunes', 3, 1, 1),
(87, '09:00', '11:00', 'Lunes', 3, 2, 2),
(88, '11:00', '13:00', 'Lunes', 3, 3, 3),
(89, '13:00', '15:00', 'Lunes', 3, 1, 4),
(90, '15:00', '17:00', 'Lunes', 3, 2, 5),
(91, '17:00', '19:00', 'Lunes', 3, 3, 6),
(92, '19:00', '21:00', 'Lunes', 3, 1, 7),
(93, '07:00', '09:00', 'Martes', 3, 1, 1),
(94, '09:00', '11:00', 'Martes', 3, 2, 2),
(95, '11:00', '13:00', 'Martes', 3, 3, 3),
(96, '13:00', '15:00', 'Martes', 3, 1, 4),
(97, '15:00', '17:00', 'Martes', 3, 2, 5),
(98, '17:00', '19:00', 'Martes', 3, 3, 6),
(99, '19:00', '21:00', 'Martes', 3, 1, 7),
(100, '07:00', '09:00', 'Miercoles', 3, 1, 1),
(101, '09:00', '11:00', 'Miercoles', 3, 2, 2),
(102, '11:00', '13:00', 'Miercoles', 3, 3, 3),
(103, '13:00', '15:00', 'Miercoles', 3, 1, 4),
(104, '15:00', '17:00', 'Miercoles', 3, 2, 5),
(105, '17:00', '19:00', 'Miercoles', 3, 3, 6),
(106, '19:00', '21:00', 'Miercoles', 3, 1, 7),
(107, '07:00', '09:00', 'Jueves', 3, 1, 1),
(108, '09:00', '11:00', 'Jueves', 3, 2, 2),
(109, '11:00', '13:00', 'Jueves', 3, 3, 3),
(110, '13:00', '15:00', 'Jueves', 3, 1, 4),
(111, '15:00', '17:00', 'Jueves', 3, 2, 5),
(112, '17:00', '19:00', 'Jueves', 3, 3, 6),
(113, '19:00', '21:00', 'Jueves', 3, 1, 7),
(114, '07:00', '09:00', 'Viernes', 3, 1, 1),
(115, '09:00', '11:00', 'Viernes', 3, 2, 2),
(116, '11:00', '13:00', 'Viernes', 3, 3, 3),
(117, '13:00', '15:00', 'Viernes', 3, 1, 4),
(118, '15:00', '17:00', 'Viernes', 3, 2, 5),
(119, '17:00', '19:00', 'Viernes', 3, 3, 6),
(120, '19:00', '21:00', 'Viernes', 3, 1, 7),
(121, '07:00', '09:00', 'Sabado', 3, 1, 1),
(122, '09:00', '11:00', 'Sabado', 3, 2, 2),
(123, '11:00', '13:00', 'Sabado', 3, 3, 3),
(124, '13:00', '15:00', 'Sabado', 3, 1, 4),
(125, '15:00', '17:00', 'Sabado', 3, 2, 5),
(126, '17:00', '19:00', 'Sabado', 3, 3, 6),
(127, '19:00', '21:00', 'Sabado', 3, 1, 7),
(128, '07:00', '09:00', 'Lunes', 4, 1, 1),
(129, '09:00', '11:00', 'Lunes', 4, 2, 2),
(130, '11:00', '13:00', 'Lunes', 4, 3, 3),
(131, '13:00', '15:00', 'Lunes', 4, 1, 4),
(132, '15:00', '17:00', 'Lunes', 4, 2, 5),
(133, '17:00', '19:00', 'Lunes', 4, 3, 6),
(134, '19:00', '21:00', 'Lunes', 4, 1, 7),
(135, '07:00', '09:00', 'Martes', 4, 1, 1),
(136, '09:00', '11:00', 'Martes', 4, 2, 2),
(137, '11:00', '13:00', 'Martes', 4, 3, 3),
(138, '13:00', '15:00', 'Martes', 4, 1, 4),
(139, '15:00', '17:00', 'Martes', 4, 2, 5),
(140, '17:00', '19:00', 'Martes', 4, 3, 6),
(141, '19:00', '21:00', 'Martes', 4, 1, 7),
(142, '07:00', '09:00', 'Miercoles', 4, 1, 1),
(143, '09:00', '11:00', 'Miercoles', 4, 2, 2),
(144, '11:00', '13:00', 'Miercoles', 4, 3, 3),
(145, '13:00', '15:00', 'Miercoles', 4, 1, 4),
(146, '15:00', '17:00', 'Miercoles', 4, 2, 5),
(147, '17:00', '19:00', 'Miercoles', 4, 3, 6),
(148, '19:00', '21:00', 'Miercoles', 4, 1, 7),
(149, '07:00', '09:00', 'Jueves', 4, 1, 1),
(150, '09:00', '11:00', 'Jueves', 4, 2, 2),
(151, '11:00', '13:00', 'Jueves', 4, 3, 3),
(152, '13:00', '15:00', 'Jueves', 4, 1, 4),
(153, '15:00', '17:00', 'Jueves', 4, 2, 5),
(154, '17:00', '19:00', 'Jueves', 4, 3, 6),
(155, '19:00', '21:00', 'Jueves', 4, 1, 7),
(156, '07:00', '09:00', 'Viernes', 4, 1, 1),
(157, '09:00', '11:00', 'Viernes', 4, 2, 2),
(158, '11:00', '13:00', 'Viernes', 4, 3, 3),
(159, '13:00', '15:00', 'Viernes', 4, 1, 4),
(160, '15:00', '17:00', 'Viernes', 4, 2, 5),
(161, '17:00', '19:00', 'Viernes', 4, 3, 6),
(162, '19:00', '21:00', 'Viernes', 4, 1, 7),
(163, '07:00', '09:00', 'Sabado', 4, 1, 1),
(164, '09:00', '11:00', 'Sabado', 4, 2, 2),
(165, '11:00', '13:00', 'Sabado', 4, 3, 3),
(166, '13:00', '15:00', 'Sabado', 4, 1, 4),
(167, '15:00', '17:00', 'Sabado', 4, 2, 5),
(168, '17:00', '19:00', 'Sabado', 4, 3, 6),
(169, '19:00', '21:00', 'Sabado', 4, 1, 7),
(170, '07:00', '09:00', 'Lunes', 5, 1, 1),
(171, '09:00', '11:00', 'Lunes', 5, 2, 2),
(172, '11:00', '13:00', 'Lunes', 5, 3, 3),
(173, '13:00', '15:00', 'Lunes', 5, 1, 4),
(174, '15:00', '17:00', 'Lunes', 5, 2, 5),
(175, '17:00', '19:00', 'Lunes', 5, 3, 6),
(176, '19:00', '21:00', 'Lunes', 5, 1, 7),
(177, '07:00', '09:00', 'Martes', 5, 1, 1),
(178, '09:00', '11:00', 'Martes', 5, 2, 2),
(179, '11:00', '13:00', 'Martes', 5, 3, 3),
(180, '13:00', '15:00', 'Martes', 5, 1, 4),
(181, '15:00', '17:00', 'Martes', 5, 2, 5),
(182, '17:00', '19:00', 'Martes', 5, 3, 6),
(183, '19:00', '21:00', 'Martes', 5, 1, 7),
(184, '07:00', '09:00', 'Miercoles', 5, 1, 1),
(185, '09:00', '11:00', 'Miercoles', 5, 2, 2),
(186, '11:00', '13:00', 'Miercoles', 5, 3, 3),
(187, '13:00', '15:00', 'Miercoles', 5, 1, 4),
(188, '15:00', '17:00', 'Miercoles', 5, 2, 5),
(189, '17:00', '19:00', 'Miercoles', 5, 3, 6),
(190, '19:00', '21:00', 'Miercoles', 5, 1, 7),
(191, '07:00', '09:00', 'Jueves', 5, 1, 1),
(192, '09:00', '11:00', 'Jueves', 5, 2, 2),
(193, '11:00', '13:00', 'Jueves', 5, 3, 3),
(194, '13:00', '15:00', 'Jueves', 5, 1, 4),
(195, '15:00', '17:00', 'Jueves', 5, 2, 5),
(196, '17:00', '19:00', 'Jueves', 5, 3, 6),
(197, '19:00', '21:00', 'Jueves', 5, 1, 7),
(198, '07:00', '09:00', 'Viernes', 5, 1, 1),
(199, '09:00', '11:00', 'Viernes', 5, 2, 2),
(200, '11:00', '13:00', 'Viernes', 5, 3, 3),
(201, '13:00', '15:00', 'Viernes', 5, 1, 4),
(202, '15:00', '17:00', 'Viernes', 5, 2, 5),
(203, '17:00', '19:00', 'Viernes', 5, 3, 6),
(204, '19:00', '21:00', 'Viernes', 5, 1, 7),
(205, '07:00', '09:00', 'Sabado', 5, 1, 1),
(206, '09:00', '11:00', 'Sabado', 5, 2, 2),
(207, '11:00', '13:00', 'Sabado', 5, 3, 3),
(208, '13:00', '15:00', 'Sabado', 5, 1, 4),
(209, '15:00', '17:00', 'Sabado', 5, 2, 5),
(210, '17:00', '19:00', 'Sabado', 5, 3, 6),
(211, '19:00', '21:00', 'Sabado', 5, 1, 7),
(212, '07:00', '09:00', 'Lunes', 6, 1, 1),
(213, '09:00', '11:00', 'Lunes', 6, 2, 2),
(214, '11:00', '13:00', 'Lunes', 6, 3, 3),
(215, '13:00', '15:00', 'Lunes', 6, 1, 4),
(216, '15:00', '17:00', 'Lunes', 6, 2, 5),
(217, '17:00', '19:00', 'Lunes', 6, 3, 6),
(218, '19:00', '21:00', 'Lunes', 6, 1, 7),
(219, '07:00', '09:00', 'Martes', 6, 1, 1),
(220, '09:00', '11:00', 'Martes', 6, 2, 2),
(221, '11:00', '13:00', 'Martes', 6, 3, 3),
(222, '13:00', '15:00', 'Martes', 6, 1, 4),
(223, '15:00', '17:00', 'Martes', 6, 2, 5),
(224, '17:00', '19:00', 'Martes', 6, 3, 6),
(225, '19:00', '21:00', 'Martes', 6, 1, 7),
(226, '07:00', '09:00', 'Miercoles', 6, 1, 1),
(227, '09:00', '11:00', 'Miercoles', 6, 2, 2),
(228, '11:00', '13:00', 'Miercoles', 6, 3, 3),
(229, '13:00', '15:00', 'Miercoles', 6, 1, 4),
(230, '15:00', '17:00', 'Miercoles', 6, 2, 5),
(231, '17:00', '19:00', 'Miercoles', 6, 3, 6),
(232, '19:00', '21:00', 'Miercoles', 6, 1, 7),
(233, '07:00', '09:00', 'Jueves', 6, 1, 1),
(234, '09:00', '11:00', 'Jueves', 6, 2, 2),
(235, '11:00', '13:00', 'Jueves', 6, 3, 3),
(236, '13:00', '15:00', 'Jueves', 6, 1, 4),
(237, '15:00', '17:00', 'Jueves', 6, 2, 5),
(238, '17:00', '19:00', 'Jueves', 6, 3, 6),
(239, '19:00', '21:00', 'Jueves', 6, 1, 7),
(240, '07:00', '09:00', 'Viernes', 6, 1, 1),
(241, '09:00', '11:00', 'Viernes', 6, 2, 2),
(242, '11:00', '13:00', 'Viernes', 6, 3, 3),
(243, '13:00', '15:00', 'Viernes', 6, 1, 4),
(244, '15:00', '17:00', 'Viernes', 6, 2, 5),
(245, '17:00', '19:00', 'Viernes', 6, 3, 6),
(246, '19:00', '21:00', 'Viernes', 6, 1, 7),
(247, '07:00', '09:00', 'Sabado', 6, 1, 1),
(248, '09:00', '11:00', 'Sabado', 6, 2, 2),
(249, '11:00', '13:00', 'Sabado', 6, 3, 3),
(250, '13:00', '15:00', 'Sabado', 6, 1, 4),
(251, '15:00', '17:00', 'Sabado', 6, 2, 5),
(252, '17:00', '19:00', 'Sabado', 6, 3, 6),
(253, '19:00', '21:00', 'Sabado', 6, 1, 7),
(254, '07:00', '09:00', 'Lunes', 7, 1, 1),
(255, '09:00', '11:00', 'Lunes', 7, 2, 2),
(256, '11:00', '13:00', 'Lunes', 7, 3, 3),
(257, '13:00', '15:00', 'Lunes', 7, 1, 4),
(258, '15:00', '17:00', 'Lunes', 7, 2, 5),
(259, '17:00', '19:00', 'Lunes', 7, 3, 6),
(260, '19:00', '21:00', 'Lunes', 7, 1, 7),
(261, '07:00', '09:00', 'Martes', 7, 1, 1),
(262, '09:00', '11:00', 'Martes', 7, 2, 2),
(263, '11:00', '13:00', 'Martes', 7, 3, 3),
(264, '13:00', '15:00', 'Martes', 7, 1, 4),
(265, '15:00', '17:00', 'Martes', 7, 2, 5),
(266, '17:00', '19:00', 'Martes', 7, 3, 6),
(267, '19:00', '21:00', 'Martes', 7, 1, 7),
(268, '07:00', '09:00', 'Miercoles', 7, 1, 1),
(269, '09:00', '11:00', 'Miercoles', 7, 2, 2),
(270, '11:00', '13:00', 'Miercoles', 7, 3, 3),
(271, '13:00', '15:00', 'Miercoles', 7, 1, 4),
(272, '15:00', '17:00', 'Miercoles', 7, 2, 5),
(273, '17:00', '19:00', 'Miercoles', 7, 3, 6),
(274, '19:00', '21:00', 'Miercoles', 7, 1, 7),
(275, '07:00', '09:00', 'Jueves', 7, 1, 1),
(276, '09:00', '11:00', 'Jueves', 7, 2, 2),
(277, '11:00', '13:00', 'Jueves', 7, 3, 3),
(278, '13:00', '15:00', 'Jueves', 7, 1, 4),
(279, '15:00', '17:00', 'Jueves', 7, 2, 5),
(280, '17:00', '19:00', 'Jueves', 7, 3, 6),
(281, '19:00', '21:00', 'Jueves', 7, 1, 7),
(282, '07:00', '09:00', 'Viernes', 7, 1, 1),
(283, '09:00', '11:00', 'Viernes', 7, 2, 2),
(284, '11:00', '13:00', 'Viernes', 7, 3, 3),
(285, '13:00', '15:00', 'Viernes', 7, 1, 4),
(286, '15:00', '17:00', 'Viernes', 7, 2, 5),
(287, '17:00', '19:00', 'Viernes', 7, 3, 6),
(288, '19:00', '21:00', 'Viernes', 7, 1, 7),
(289, '07:00', '09:00', 'Sabado', 7, 1, 1),
(290, '09:00', '11:00', 'Sabado', 7, 2, 2),
(291, '11:00', '13:00', 'Sabado', 7, 3, 3),
(292, '13:00', '15:00', 'Sabado', 7, 1, 4),
(293, '15:00', '17:00', 'Sabado', 7, 2, 5),
(294, '17:00', '19:00', 'Sabado', 7, 3, 6),
(295, '19:00', '21:00', 'Sabado', 7, 1, 7),
(296, '07:00', '09:00', 'Lunes', 8, 1, 1),
(297, '09:00', '11:00', 'Lunes', 8, 2, 2),
(298, '11:00', '13:00', 'Lunes', 8, 3, 3),
(299, '13:00', '15:00', 'Lunes', 8, 1, 4),
(300, '15:00', '17:00', 'Lunes', 8, 2, 5),
(301, '17:00', '19:00', 'Lunes', 8, 3, 6),
(302, '19:00', '21:00', 'Lunes', 8, 1, 7),
(303, '07:00', '09:00', 'Martes', 8, 1, 1),
(304, '09:00', '11:00', 'Martes', 8, 2, 2),
(305, '11:00', '13:00', 'Martes', 8, 3, 3),
(306, '13:00', '15:00', 'Martes', 8, 1, 4),
(307, '15:00', '17:00', 'Martes', 8, 2, 5),
(308, '17:00', '19:00', 'Martes', 8, 3, 6),
(309, '19:00', '21:00', 'Martes', 8, 1, 7),
(310, '07:00', '09:00', 'Miercoles', 8, 1, 1),
(311, '09:00', '11:00', 'Miercoles', 8, 2, 2),
(312, '11:00', '13:00', 'Miercoles', 8, 3, 3),
(313, '13:00', '15:00', 'Miercoles', 8, 1, 4),
(314, '15:00', '17:00', 'Miercoles', 8, 2, 5),
(315, '17:00', '19:00', 'Miercoles', 8, 3, 6),
(316, '19:00', '21:00', 'Miercoles', 8, 1, 7),
(317, '07:00', '09:00', 'Jueves', 8, 1, 1),
(318, '09:00', '11:00', 'Jueves', 8, 2, 2),
(319, '11:00', '13:00', 'Jueves', 8, 3, 3),
(320, '13:00', '15:00', 'Jueves', 8, 1, 4),
(321, '15:00', '17:00', 'Jueves', 8, 2, 5),
(322, '17:00', '19:00', 'Jueves', 8, 3, 6),
(323, '19:00', '21:00', 'Jueves', 8, 1, 7),
(324, '07:00', '09:00', 'Viernes', 8, 1, 1),
(325, '09:00', '11:00', 'Viernes', 8, 2, 2),
(326, '11:00', '13:00', 'Viernes', 8, 3, 3),
(327, '13:00', '15:00', 'Viernes', 8, 1, 4),
(328, '15:00', '17:00', 'Viernes', 8, 2, 5),
(329, '17:00', '19:00', 'Viernes', 8, 3, 6),
(330, '19:00', '21:00', 'Viernes', 8, 1, 7),
(331, '07:00', '09:00', 'Sabado', 8, 1, 1),
(332, '09:00', '11:00', 'Sabado', 8, 2, 2),
(333, '11:00', '13:00', 'Sabado', 8, 3, 3),
(334, '13:00', '15:00', 'Sabado', 8, 1, 4),
(335, '15:00', '17:00', 'Sabado', 8, 2, 5),
(336, '17:00', '19:00', 'Sabado', 8, 3, 6),
(337, '19:00', '21:00', 'Sabado', 8, 1, 7),
(338, '07:00', '09:00', 'Lunes', 9, 1, 1),
(339, '09:00', '11:00', 'Lunes', 9, 2, 2),
(340, '11:00', '13:00', 'Lunes', 9, 3, 3),
(341, '13:00', '15:00', 'Lunes', 9, 1, 4),
(342, '15:00', '17:00', 'Lunes', 9, 2, 5),
(343, '17:00', '19:00', 'Lunes', 9, 3, 6),
(344, '19:00', '21:00', 'Lunes', 9, 1, 7),
(345, '07:00', '09:00', 'Martes', 9, 1, 1),
(346, '09:00', '11:00', 'Martes', 9, 2, 2),
(347, '11:00', '13:00', 'Martes', 9, 3, 3),
(348, '13:00', '15:00', 'Martes', 9, 1, 4),
(349, '15:00', '17:00', 'Martes', 9, 2, 5),
(350, '17:00', '19:00', 'Martes', 9, 3, 6),
(351, '19:00', '21:00', 'Martes', 9, 1, 7),
(352, '07:00', '09:00', 'Miercoles', 9, 1, 1),
(353, '09:00', '11:00', 'Miercoles', 9, 2, 2),
(354, '11:00', '13:00', 'Miercoles', 9, 3, 3),
(355, '13:00', '15:00', 'Miercoles', 9, 1, 4),
(356, '15:00', '17:00', 'Miercoles', 9, 2, 5),
(357, '17:00', '19:00', 'Miercoles', 9, 3, 6),
(358, '19:00', '21:00', 'Miercoles', 9, 1, 7),
(359, '07:00', '09:00', 'Jueves', 9, 1, 1),
(360, '09:00', '11:00', 'Jueves', 9, 2, 2),
(361, '11:00', '13:00', 'Jueves', 9, 3, 3),
(362, '13:00', '15:00', 'Jueves', 9, 1, 4),
(363, '15:00', '17:00', 'Jueves', 9, 2, 5),
(364, '17:00', '19:00', 'Jueves', 9, 3, 6),
(365, '19:00', '21:00', 'Jueves', 9, 1, 7),
(366, '07:00', '09:00', 'Viernes', 9, 1, 1),
(367, '09:00', '11:00', 'Viernes', 9, 2, 2),
(368, '11:00', '13:00', 'Viernes', 9, 3, 3),
(369, '13:00', '15:00', 'Viernes', 9, 1, 4),
(370, '15:00', '17:00', 'Viernes', 9, 2, 5),
(371, '17:00', '19:00', 'Viernes', 9, 3, 6),
(372, '19:00', '21:00', 'Viernes', 9, 1, 7),
(373, '07:00', '09:00', 'Sabado', 9, 1, 1),
(374, '09:00', '11:00', 'Sabado', 9, 2, 2),
(375, '11:00', '13:00', 'Sabado', 9, 3, 3),
(376, '13:00', '15:00', 'Sabado', 9, 1, 4),
(377, '15:00', '17:00', 'Sabado', 9, 2, 5),
(378, '17:00', '19:00', 'Sabado', 9, 3, 6),
(379, '19:00', '21:00', 'Sabado', 9, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_marcadorescompartidos`
--

CREATE TABLE `ar_server_marcadorescompartidos` (
  `id` int(11) NOT NULL,
  `user_emisor` varchar(20) NOT NULL,
  `user_receptor` varchar(20) NOT NULL,
  `CIC_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_materia`
--

CREATE TABLE `ar_server_materia` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `carrera` varchar(10) NOT NULL,
  `clave` varchar(20) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `semestre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_materia`
--

INSERT INTO `ar_server_materia` (`id`, `nombre`, `carrera`, `clave`, `descripcion`, `semestre`) VALUES
(1, 'Ingeneria en software', 'SOF11', '000001', '', '2018-2'),
(2, 'Logica digital', 'SOF11', '000002', '', '2018-2'),
(3, 'Algebra lineal', 'LAT11', '000003', '', '2018-2'),
(4, 'Dispositivos Logicos', 'SOF11', '000004', '', '2018-2'),
(5, 'Programacion orientada a objetos', 'SOF11', '000005', '', '2018-2'),
(6, 'Introduccion a la programacion', 'SOF11', '000006', '', '2018-2'),
(7, 'Calculo', 'LAT11', '000007', '', '2018-2');

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_puntointeres`
--

CREATE TABLE `ar_server_puntointeres` (
  `id` int(11) NOT NULL,
  `latitud` varchar(30) NOT NULL,
  `longitud` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_puntointeres`
--

INSERT INTO `ar_server_puntointeres` (`id`, `latitud`, `longitud`, `user_id`) VALUES
(1, '-', '-', 666),
(2, '-', '-', 258858);

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_roles`
--

CREATE TABLE `ar_server_roles` (
  `id` int(11) NOT NULL,
  `rol` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_roles`
--

INSERT INTO `ar_server_roles` (`id`, `rol`) VALUES
(1, 'Alumno'),
(2, 'Docente'),
(3, 'Administrador'),
(4, 'AdmAdministrativo');

-- --------------------------------------------------------

--
-- Table structure for table `ar_server_solicitudamistad`
--

CREATE TABLE `ar_server_solicitudamistad` (
  `id` int(11) NOT NULL,
  `estatus` varchar(15) NOT NULL,
  `user_emisor` varchar(20) NOT NULL,
  `user_receptor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_server_solicitudamistad`
--

INSERT INTO `ar_server_solicitudamistad` (`id`, `estatus`, `user_emisor`, `user_receptor`) VALUES
(1, 'Aceptada', '258858', '259615'),
(2, 'Pendiente', '258858', '258834'),
(3, 'Pendiente', '258858', '258853'),
(4, 'Aceptada', '258858', '258846'),
(5, 'Rechazada', '238253', '258858'),
(6, 'Aceptada', '258846', '238253'),
(7, 'Aceptada', '235838', '258858'),
(8, 'Aceptada', '258858', '267802'),
(9, 'Pendiente', '258858', '258858'),
(10, 'Pendiente', '234643', '258858'),
(11, 'Pendiente', '234643', '235838'),
(12, 'Aceptada', '261732', '234643'),
(13, 'Pendiente', '234643', '258841');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add group', 2, 'add_group'),
(5, 'Can change group', 2, 'change_group'),
(6, 'Can delete group', 2, 'delete_group'),
(7, 'Can add permission', 3, 'add_permission'),
(8, 'Can change permission', 3, 'change_permission'),
(9, 'Can delete permission', 3, 'delete_permission'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add amigos', 7, 'add_amigos'),
(20, 'Can change amigos', 7, 'change_amigos'),
(21, 'Can delete amigos', 7, 'delete_amigos'),
(22, 'Can add datos user', 8, 'add_datosuser'),
(23, 'Can change datos user', 8, 'change_datosuser'),
(24, 'Can delete datos user', 8, 'delete_datosuser'),
(25, 'Can add punto interes', 9, 'add_puntointeres'),
(26, 'Can change punto interes', 9, 'change_puntointeres'),
(27, 'Can delete punto interes', 9, 'delete_puntointeres'),
(28, 'Can add docente', 10, 'add_docente'),
(29, 'Can change docente', 10, 'change_docente'),
(30, 'Can delete docente', 10, 'delete_docente'),
(31, 'Can add campo informativo contenido', 11, 'add_campoinformativocontenido'),
(32, 'Can change campo informativo contenido', 11, 'change_campoinformativocontenido'),
(33, 'Can delete campo informativo contenido', 11, 'delete_campoinformativocontenido'),
(34, 'Can add solicitud amistad', 12, 'add_solicitudamistad'),
(35, 'Can change solicitud amistad', 12, 'change_solicitudamistad'),
(36, 'Can delete solicitud amistad', 12, 'delete_solicitudamistad'),
(37, 'Can add comentario', 13, 'add_comentario'),
(38, 'Can change comentario', 13, 'change_comentario'),
(39, 'Can delete comentario', 13, 'delete_comentario'),
(40, 'Can add calificacion', 14, 'add_calificacion'),
(41, 'Can change calificacion', 14, 'change_calificacion'),
(42, 'Can delete calificacion', 14, 'delete_calificacion'),
(43, 'Can add roles', 15, 'add_roles'),
(44, 'Can change roles', 15, 'change_roles'),
(45, 'Can delete roles', 15, 'delete_roles'),
(46, 'Can add materia', 16, 'add_materia'),
(47, 'Can change materia', 16, 'change_materia'),
(48, 'Can delete materia', 16, 'delete_materia'),
(49, 'Can add campo informativo lista', 17, 'add_campoinformativolista'),
(50, 'Can change campo informativo lista', 17, 'change_campoinformativolista'),
(51, 'Can delete campo informativo lista', 17, 'delete_campoinformativolista'),
(52, 'Can add aula', 18, 'add_aula'),
(53, 'Can change aula', 18, 'change_aula'),
(54, 'Can delete aula', 18, 'delete_aula'),
(55, 'Can add horario', 19, 'add_horario'),
(56, 'Can change horario', 19, 'change_horario'),
(57, 'Can delete horario', 19, 'delete_horario'),
(58, 'Can add documentos', 20, 'add_documentos'),
(59, 'Can change documentos', 20, 'change_documentos'),
(60, 'Can delete documentos', 20, 'delete_documentos'),
(61, 'Can add ci c_cil', 21, 'add_cic_cil'),
(62, 'Can change ci c_cil', 21, 'change_cic_cil'),
(63, 'Can delete ci c_cil', 21, 'delete_cic_cil'),
(64, 'Can add marcadores compartidos', 22, 'add_marcadorescompartidos'),
(65, 'Can change marcadores compartidos', 22, 'change_marcadorescompartidos'),
(66, 'Can delete marcadores compartidos', 22, 'delete_marcadorescompartidos'),
(67, 'Can add ci l_cil', 23, 'add_cil_cil'),
(68, 'Can change ci l_cil', 23, 'change_cil_cil'),
(69, 'Can delete ci l_cil', 23, 'delete_cil_cil'),
(70, 'Can add seccion', 24, 'add_seccion'),
(71, 'Can change seccion', 24, 'change_seccion'),
(72, 'Can delete seccion', 24, 'delete_seccion'),
(73, 'Can add activos', 25, 'add_activos'),
(74, 'Can change activos', 25, 'change_activos'),
(75, 'Can delete activos', 25, 'delete_activos'),
(76, 'Can add prestamos', 26, 'add_prestamos'),
(77, 'Can change prestamos', 26, 'change_prestamos'),
(78, 'Can delete prestamos', 26, 'delete_prestamos');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(666, '666', NULL, 1, 'Admin', '-', '-', '-', 0, 1, '0000-00-00 00:00:00.000000'),
(234643, 'pbkdf2_sha256$36000$oRskWxbbo9p0$MUxNzcoewNEqJJxFd7WQr66KnLA78MlfBn1n74DO9xY=', NULL, 0, 'Perla', 'PERLA ELIZABETH', 'AGUILAR MALDONADO', 'elizabeth_kr@hotmail.com', 0, 1, '2018-12-11 20:04:12.973000'),
(235838, 'pbkdf2_sha256$36000$UuWrPnnsQM1y$buYvRZIreanoReWWDbcTijZNsxsUE9IJOtQlhz1uOe4=', NULL, 0, 'jorggerojas', 'JORGE LUIS', 'ROJAS ARCOS', 'jorgelroar@gmail.com', 0, 1, '2018-12-10 22:12:49.838000'),
(238253, 'pbkdf2_sha256$36000$uCqFGQHlNFCB$dTWSxdStE8TFWEC3DtV8HiVUCPiWcUsRfJSAel8Cu30=', NULL, 0, 'Trihexa64', ' GERARDO', 'GUDIÑO GARCIA', 'areg_4@hotmail.com', 0, 1, '2018-11-27 16:05:43.402000'),
(258834, 'pbkdf2_sha256$36000$R4RNhLLKLHHj$bdDZrei4p+i27YhgU3FIAohN6JZtnyA4z05GCSjsxHA=', NULL, 0, 'El bug', 'ALEJANDRA ESTEFANIA', 'MORALES BECERRA', 'alemoralesb11@gmail.com', 0, 1, '2018-11-25 21:45:36.869000'),
(258841, 'pbkdf2_sha256$36000$8voeftPlnR1M$AcnJ2VA0pxp608JdFC06pN2fsrrWob2GEpsmDRznpSU=', NULL, 0, 'LuisF', 'LUIS FABIEL', 'MARTINEZ HERNANDEZ', 'Luis@gmail.com', 0, 1, '2018-12-08 22:59:26.242000'),
(258842, 'pbkdf2_sha256$36000$VysUGTqHBwNB$qQH2EReeU6lZydK6Nn5vAgJQjrqPw4LEi7vt0qEzLeg=', NULL, 0, 'FernandoRAM', 'FERNANDO ', 'RINCON AMAYA', 'fernandoramaya9@gmail.com', 0, 1, '2018-11-27 23:35:51.809000'),
(258846, 'pbkdf2_sha256$36000$jPvSSb9i6h18$3yqRaNUD33Uh9FM5tTerFq8M18Z4wKZnMiGlCq+Q3KU=', NULL, 0, 'Palomeca', 'JONATHAN ', 'PALOMEQUE REBOLLO', 'john@gmail.com', 0, 1, '2018-11-25 04:26:39.245000'),
(258853, 'pbkdf2_sha256$36000$NrHanV4wY02d$xcsUDQzRbO+AlS4bW3XusLNJ7UintIE3EMQS5TSK2xU=', NULL, 0, 'Daniel', 'DANIEL ', 'SIERRA REVELES', 'Dante@gmail.com', 0, 1, '2018-11-26 17:32:36.140000'),
(258857, 'pbkdf2_sha256$36000$qam51DRhdcdV$M4pJQZxvWL8d3lZ9jI5uwkvBBjuh0gK0tu9bqyVcDS8=', NULL, 0, 'Jacqui', 'ANA JACQUELINE', 'MORALES CONTRERAS', 'Jacqueline@gmail.com', 0, 1, '2018-11-26 16:34:52.869000'),
(258858, 'pbkdf2_sha256$36000$bPhQl3QRafRB$4VVb8jO2sk7BxV9DV89FBoAZzJyzMtaZbWmesIN0GvI=', NULL, 0, 'CarlosD', 'CARLOS DANIEL', 'ESTRADA GONZALEZ', 'carlosd_eg@yahoo.com.mx', 0, 1, '2018-11-25 04:15:36.550000'),
(259615, 'pbkdf2_sha256$36000$kIVJKaMwnEaT$b4iGOln5NBQOhfd/X/poMb930RlPVVfy442vdz/RI1c=', NULL, 0, 'Axel_98', 'AXEL ANTOINE', 'CASTILLO CADENAS', 'axelcadenas4@gmail.com', 0, 1, '2018-11-26 21:22:21.349000'),
(261732, 'pbkdf2_sha256$36000$tSflZP0mnEUL$HY5xhrukDDmXngedMJCDpvWCvmRIsmVVlcrSEX+dXUg=', NULL, 0, 'Sharol', 'SHAROL RUBI', 'GONZALEZ SERRANO', 'sharol9813@gmail.com', 0, 1, '2018-12-11 20:06:03.202000'),
(267785, 'pbkdf2_sha256$36000$SzmEHxvp59Oc$F6jo9F+VGzM45+JEx4ke7eLfcY/DjtKn0c1U0p1W1Eg=', NULL, 0, 'PU55YD35TR0Y3R69', 'PABLO', 'SAMANO ELTON', 'psamano25@alumnos.uaq.mx', 0, 1, '2018-11-27 23:46:41.905000'),
(267802, 'pbkdf2_sha256$36000$gDodRWVwB1BC$zhVwlaUwlfSxkJCjKMBk0wiEz5GAvWk+tbmXqgK6I2Y=', NULL, 0, 'antoniovgmx', 'ANTONIO', 'VAZQUEZ GUTIERREZ', 'antoniovg.mx@gmail.com', 0, 1, '2018-11-27 23:41:10.502000'),
(278795, 'pbkdf2_sha256$36000$TVTOLXnNcIaA$DaPCBIs8ur+/AmpyZE4jecn8KNc5e7E7KOxdUsSWcZE=', NULL, 0, 'ThinGirl', 'DAFNE', 'RUIZ PEREZ', 'Dafne@gmail.com', 0, 1, '2018-11-26 03:30:19.903000'),
(285468, 'gg', '0000-00-00 00:00:00.000000', 0, 'LexVargas', 'Jose Alejandro ', 'Vargas Diaz', 'lexvargas@gmail.com', 0, 1, '2018-11-22 01:35:45.305000'),
(285469, 'ff', NULL, 0, 'LaloC', 'Eduardo', 'Aguirre Caracheo', 'lalo@gmail.com', 0, 1, '0000-00-00 00:00:00.000000'),
(285470, 'hh', NULL, 0, 'Dieguito', 'Diego Octavio', 'Ibarra Corona', 'dieguito@gmail.com', 0, 1, '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(7, 'AR_server', 'amigos'),
(18, 'AR_server', 'aula'),
(14, 'AR_server', 'calificacion'),
(11, 'AR_server', 'campoinformativocontenido'),
(17, 'AR_server', 'campoinformativolista'),
(21, 'AR_server', 'cic_cil'),
(23, 'AR_server', 'cil_cil'),
(13, 'AR_server', 'comentario'),
(8, 'AR_server', 'datosuser'),
(10, 'AR_server', 'docente'),
(20, 'AR_server', 'documentos'),
(19, 'AR_server', 'horario'),
(22, 'AR_server', 'marcadorescompartidos'),
(16, 'AR_server', 'materia'),
(9, 'AR_server', 'puntointeres'),
(15, 'AR_server', 'roles'),
(12, 'AR_server', 'solicitudamistad'),
(2, 'auth', 'group'),
(3, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(25, 'Prestamos', 'activos'),
(26, 'Prestamos', 'prestamos'),
(24, 'Prestamos', 'seccion'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2018-11-24 12:02:17.740000'),
(2, 'auth', '0001_initial', '2018-11-24 12:02:30.764000'),
(3, 'AR_server', '0001_initial', '2018-11-24 12:03:00.138000'),
(4, 'admin', '0001_initial', '2018-11-24 12:03:06.561000'),
(5, 'admin', '0002_logentry_remove_auto_add', '2018-11-24 12:03:06.753000'),
(6, 'contenttypes', '0002_remove_content_type_name', '2018-11-24 12:03:08.937000'),
(7, 'auth', '0002_alter_permission_name_max_length', '2018-11-24 12:03:09.996000'),
(8, 'auth', '0003_alter_user_email_max_length', '2018-11-24 12:03:11.306000'),
(9, 'auth', '0004_alter_user_username_opts', '2018-11-24 12:03:11.462000'),
(10, 'auth', '0005_alter_user_last_login_null', '2018-11-24 12:03:12.444000'),
(11, 'auth', '0006_require_contenttypes_0002', '2018-11-24 12:03:12.527000'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2018-11-24 12:03:12.666000'),
(13, 'auth', '0008_alter_user_username_max_length', '2018-11-24 12:03:14.269000'),
(14, 'sessions', '0001_initial', '2018-11-24 12:03:15.212000'),
(15, 'AR_server', '0002_auto_20181202_0038', '2018-12-02 06:45:36.163000'),
(16, 'AR_server', '0003_comentario_id_clase_docente', '2018-12-02 08:45:59.043000'),
(17, 'AR_server', '0004_auto_20181208_2104', '2018-12-09 03:05:16.297000'),
(18, 'AR_server', '0005_auto_20181209_0208', '2018-12-09 08:09:03.690000'),
(19, 'Prestamos', '0001_initial', '2018-12-13 08:55:47.170000');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ar_server_amigos`
--
ALTER TABLE `ar_server_amigos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ar_server_aula`
--
ALTER TABLE `ar_server_aula`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ar_server_calificacion`
--
ALTER TABLE `ar_server_calificacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_calificacion_user_id_6c7c92e6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `ar_server_campoinformativocontenido`
--
ALTER TABLE `ar_server_campoinformativocontenido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_campoinfor_puntoInteres_id_5f290dd6_fk_AR_server` (`puntoInteres_id`);

--
-- Indexes for table `ar_server_campoinformativolista`
--
ALTER TABLE `ar_server_campoinformativolista`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_campoinfor_puntoInteres_id_8608d590_fk_AR_server` (`puntoInteres_id`);

--
-- Indexes for table `ar_server_cic_cil`
--
ALTER TABLE `ar_server_cic_cil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_cic_cil_CIC_id_8cb99002_fk_AR_server` (`CIC_id`);

--
-- Indexes for table `ar_server_cil_cil`
--
ALTER TABLE `ar_server_cil_cil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ar_server_comentario`
--
ALTER TABLE `ar_server_comentario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_comentario_user_id_169bd98d_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `ar_server_datosuser`
--
ALTER TABLE `ar_server_datosuser`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_datosuser_rol_id_cd5b09bd_fk_AR_server_roles_id` (`rol_id`),
  ADD KEY `AR_server_datosuser_user_id_5e132de5_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `ar_server_docente`
--
ALTER TABLE `ar_server_docente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_docente_user_id_2dfd59b3_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `ar_server_documentos`
--
ALTER TABLE `ar_server_documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_documentos_idDocente_id_c9e543df_fk_auth_user_id` (`idDocente_id`);

--
-- Indexes for table `ar_server_horario`
--
ALTER TABLE `ar_server_horario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_horario_aula_id_4fd7410a_fk_AR_server_aula_id` (`aula_id`),
  ADD KEY `AR_server_horario_docente_id_ce30b4cd_fk_AR_server_docente_id` (`docente_id`),
  ADD KEY `AR_server_horario_materia_id_c99b0c09_fk_AR_server_materia_id` (`materia_id`);

--
-- Indexes for table `ar_server_marcadorescompartidos`
--
ALTER TABLE `ar_server_marcadorescompartidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_marcadores_CIC_id_1fa064b9_fk_AR_server` (`CIC_id`);

--
-- Indexes for table `ar_server_materia`
--
ALTER TABLE `ar_server_materia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ar_server_puntointeres`
--
ALTER TABLE `ar_server_puntointeres`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AR_server_puntointeres_user_id_06ac52b8_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `ar_server_roles`
--
ALTER TABLE `ar_server_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ar_server_solicitudamistad`
--
ALTER TABLE `ar_server_solicitudamistad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ar_server_amigos`
--
ALTER TABLE `ar_server_amigos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ar_server_aula`
--
ALTER TABLE `ar_server_aula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `ar_server_calificacion`
--
ALTER TABLE `ar_server_calificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ar_server_campoinformativocontenido`
--
ALTER TABLE `ar_server_campoinformativocontenido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ar_server_campoinformativolista`
--
ALTER TABLE `ar_server_campoinformativolista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ar_server_cic_cil`
--
ALTER TABLE `ar_server_cic_cil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ar_server_cil_cil`
--
ALTER TABLE `ar_server_cil_cil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ar_server_comentario`
--
ALTER TABLE `ar_server_comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ar_server_datosuser`
--
ALTER TABLE `ar_server_datosuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `ar_server_docente`
--
ALTER TABLE `ar_server_docente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ar_server_documentos`
--
ALTER TABLE `ar_server_documentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ar_server_horario`
--
ALTER TABLE `ar_server_horario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=380;
--
-- AUTO_INCREMENT for table `ar_server_marcadorescompartidos`
--
ALTER TABLE `ar_server_marcadorescompartidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ar_server_materia`
--
ALTER TABLE `ar_server_materia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ar_server_puntointeres`
--
ALTER TABLE `ar_server_puntointeres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ar_server_roles`
--
ALTER TABLE `ar_server_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ar_server_solicitudamistad`
--
ALTER TABLE `ar_server_solicitudamistad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=285471;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ar_server_calificacion`
--
ALTER TABLE `ar_server_calificacion`
  ADD CONSTRAINT `AR_server_calificacion_user_id_6c7c92e6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `ar_server_campoinformativocontenido`
--
ALTER TABLE `ar_server_campoinformativocontenido`
  ADD CONSTRAINT `AR_server_campoinfor_puntoInteres_id_5f290dd6_fk_AR_server` FOREIGN KEY (`puntoInteres_id`) REFERENCES `ar_server_puntointeres` (`id`);

--
-- Constraints for table `ar_server_campoinformativolista`
--
ALTER TABLE `ar_server_campoinformativolista`
  ADD CONSTRAINT `AR_server_campoinfor_puntoInteres_id_8608d590_fk_AR_server` FOREIGN KEY (`puntoInteres_id`) REFERENCES `ar_server_puntointeres` (`id`);

--
-- Constraints for table `ar_server_cic_cil`
--
ALTER TABLE `ar_server_cic_cil`
  ADD CONSTRAINT `AR_server_cic_cil_CIC_id_8cb99002_fk_AR_server` FOREIGN KEY (`CIC_id`) REFERENCES `ar_server_campoinformativocontenido` (`id`);

--
-- Constraints for table `ar_server_comentario`
--
ALTER TABLE `ar_server_comentario`
  ADD CONSTRAINT `AR_server_comentario_user_id_169bd98d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `ar_server_datosuser`
--
ALTER TABLE `ar_server_datosuser`
  ADD CONSTRAINT `AR_server_datosuser_rol_id_cd5b09bd_fk_AR_server_roles_id` FOREIGN KEY (`rol_id`) REFERENCES `ar_server_roles` (`id`),
  ADD CONSTRAINT `AR_server_datosuser_user_id_5e132de5_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `ar_server_docente`
--
ALTER TABLE `ar_server_docente`
  ADD CONSTRAINT `AR_server_docente_user_id_2dfd59b3_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `ar_server_documentos`
--
ALTER TABLE `ar_server_documentos`
  ADD CONSTRAINT `AR_server_documentos_idDocente_id_c9e543df_fk_auth_user_id` FOREIGN KEY (`idDocente_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `ar_server_horario`
--
ALTER TABLE `ar_server_horario`
  ADD CONSTRAINT `AR_server_horario_aula_id_4fd7410a_fk_AR_server_aula_id` FOREIGN KEY (`aula_id`) REFERENCES `ar_server_aula` (`id`),
  ADD CONSTRAINT `AR_server_horario_docente_id_ce30b4cd_fk_AR_server_docente_id` FOREIGN KEY (`docente_id`) REFERENCES `ar_server_docente` (`id`),
  ADD CONSTRAINT `AR_server_horario_materia_id_c99b0c09_fk_AR_server_materia_id` FOREIGN KEY (`materia_id`) REFERENCES `ar_server_materia` (`id`);

--
-- Constraints for table `ar_server_marcadorescompartidos`
--
ALTER TABLE `ar_server_marcadorescompartidos`
  ADD CONSTRAINT `AR_server_marcadores_CIC_id_1fa064b9_fk_AR_server` FOREIGN KEY (`CIC_id`) REFERENCES `ar_server_campoinformativocontenido` (`id`);

--
-- Constraints for table `ar_server_puntointeres`
--
ALTER TABLE `ar_server_puntointeres`
  ADD CONSTRAINT `AR_server_puntointeres_user_id_06ac52b8_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
