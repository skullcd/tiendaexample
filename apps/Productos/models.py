# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Producto(models.Model):
    nombre_producto = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=300)
    costo = models.IntegerField()
    Disponible = models.BooleanField()
    numero_existencias = models.IntegerField()
    categoria = models.ForeignKey('Categoria')

class Categoria(models.Model):
    nombre_categoria = models.CharField(max_length=50)
    fecha_creacion = models.DateField()
