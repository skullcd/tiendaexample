# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from .models import *
from django.views.generic.list import ListView
from .forms import ProductoForm, CategoriaForm
import json
# from apps.Productos import models
# Create your views here.


def index_view(request):
    datos = []
    diccionario={}
    Productos = Producto.objects.all()
    return render(request, "index.html", {"diccionario":Productos})


def compras_view(request):
    data=[]
    Productos = Producto.objects.all()
    for datos in Productos:
        print datos.categoria.pk
        datos_categoria = Categoria.objects.filter(pk=int(datos.categoria.pk)).first()
        data.append([datos.nombre_producto, datos.descripcion, datos.costo, datos.numero_existencias, datos_categoria.nombre_categoria, datos.pk])
    return render(request, "compras.html", {"diccionario":data})


class Categorias(ListView):
    model = Categoria
    template_name = 'categorias.html'
    queryset = Categoria.objects.all()


def nuevo_producto(request):
    if request.method == 'POST':
        print "entro"
        form = ProductoForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('productos:index_view')
    else:
        form = ProductoForm()
        print form
    return render(request, "productoFormulario.html",{'form':form})


def editar_producto(request, idProducto):
    producto = Producto.objects.get(pk=idProducto)
    if request.method == 'GET':
        form = ProductoForm(instance=producto)
    else:
        form = ProductoForm(request.POST, instance=producto)
        if form.is_valid():
            form.save()
        return redirect('productos:index_view')
    return render(request,  "productoFormulario.html",{'form':form})

def eliminar_producto(request, idProducto):
    productos = Producto.objects.get(pk=idProducto)
    productos.delete()
    return redirect('productos:index_view')


def nueva_categoria(request):
    if request.method == 'POST':
        form = CategoriaForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('productos:categorias')
    else:
        form = CategoriaForm()
    return render(request, "categoriaFormulario.html",{'form':form})

def editar_categoria(request, idCategoria):
    categoria = Categoria.objects.get(pk=idCategoria)
    if request.method == 'GET':
        form = CategoriaForm(instance=categoria)
    else:
        form = CategoriaForm(request.POST, instance=categoria)
        if form.is_valid():
            form.save()
        return redirect('productos:categorias')
    return render(request,  "categoriaFormulario.html",{'form':form})

def eliminar_categoria(request, idCategoria):
    categoria = Categoria.objects.get(pk=idCategoria)
    categoria.delete()
    return redirect('productos:categorias')


def realizar_compra(request):
    datos = request.GET.get("productos")
    datos = json.loads(datos)
    # print datos
    for value in datos:
        producto = Producto.objects.filter(pk=value[0]).first()
        cantidad = producto.numero_existencias
        nueva_cantidad = int(cantidad)- int(datos[value[0]]["cantidad"])
        if nueva_cantidad < 0:
            estatus = "No hay suficientes productos"
        else:
            producto.numero_existencias = nueva_cantidad
            producto.save()
            estatus="OK"
    return HttpResponse(estatus)
