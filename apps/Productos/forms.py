from django  import forms
from .models import *

class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto
        fields = [
            'nombre_producto',
            'descripcion',
            'costo',
            'Disponible',
            'numero_existencias',
            'categoria'
        ]

        labels = {
            'nombre_producto': 'Nombre',
            'descripcion': 'Descripcion',
            'costo': 'Costo',
            'Disponible': 'Disponible',
            'numero_existencias': 'Numero existencias',
            'categoria':'Categoria',
        }
        widgets = {
            'nombre_producto':forms.TextInput(attrs={'class': 'form-control'}),
            'descripcion':forms.TextInput(attrs={'class': 'form-control'}),
            'costo':forms.NumberInput(attrs={'class': 'form-control'}),
            'Disponible':forms.NumberInput(attrs={'class': 'form-control'}),
            'numero_existencias':forms.NumberInput(attrs={'class': 'form-control'}),
            'categoria':forms.Select(attrs={'class': 'form-control'}),
            # 'categoria':forms.ModelMultipleChoiceField(queryset=Categoria.objects.all())
        }


class CategoriaForm(forms.ModelForm):
    class Meta:
        model = Categoria
        fields=[
            'nombre_categoria',
            'fecha_creacion',
        ]

        labels = {
            'nombre_categoria':'Nombre Categoria',
            'fecha_creacion':'Fecha Creacion',
        }

        widgets = {
            'nombre_categoria':forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_creacion':forms.TextInput(attrs={'class': 'form-control'}),
        }
