from django.conf.urls import url, include
from AR_server import views
urlpatterns = [

    url(r'^Teacher_view/$', views.Teacher_view, name="Teacher_view"),
    url(r'^Teacher_view/upload_document$', views.upload_document, name="upload_document"),
    url(r'^Teacher_view/get_documents_teacher_web$', views.get_documents_teacher_web, name="get_documents_teacher_web"),

    url(r'^get_friends$', views.get_friends, name="get_friends"),
    url(r'^get_friend_request$', views.get_friend_request, name="get_friend_request"),
    url(r'^login$', views.authenticate_login, name="authenticate_login"),
    url(r'^accept_request$', views.accept_request, name="accept_request"),
    url(r'^reject_request$', views.reject_request, name="reject_request"),
    url(r'^delete_friend$', views.delete_friend, name="delete_friend"),
    url(r'^send_friend_request$', views.send_friend_request, name="send_friend_request"),
    url(r'^get_profile_information$', views.get_profile_information, name="get_profile_information"),
    url(r'^create_user$', views.create_user, name="create_user"),
    url(r'^validate_user$', views.validate_user, name="validate_user"),
    url(r'^get_user_search$', views.get_user_search, name="get_user_search"),
    url(r'^get_friends_search$', views.get_friends_search, name="get_friends_search"),
    url(r'^get_information_classroom$', views.get_information_classroom, name="get_information_classroom"),
    url(r'^get_profile_data$', views.get_profile_data, name="get_profile_data"),
    url(r'^get_classroom$', views.get_classroom, name="get_classroom"),
    url(r'^get_lessons$', views.get_lessons, name="get_lessons"),
    url(r'^get_teachers$', views.get_teachers, name="get_teachers"),
    url(r'^create_comment_lesson$', views.create_comment_lesson, name="create_comment_lesson"),
    url(r'^create_comment_teacher$', views.create_comment_teacher, name="create_comment_teacher"),
    url(r'^get_comments_class$', views.get_comments_class, name="get_comments_class"),
    url(r'^get_comments_teacher$', views.get_comments_teacher, name="get_comments_teacher"),

    url(r'^get_main_markers$', views.get_main_markers, name="get_main_markers"),
    url(r'^add_marker$', views.add_marker, name="add_marker"),
    url(r'^get_markers_user$', views.get_markers_user, name="get_markers_user"),
    url(r'^delete_marker$', views.delete_marker, name="delete_marker"),
    url(r'^get_information_main_marker$', views.get_information_main_marker, name="get_information_main_marker"),
    url(r'^get_user_shared$', views.get_user_shared, name="get_user_shared"),
    url(r'^share_marker$', views.share_marker, name="share_marker"),
    url(r'^delete_share_marker$', views.delete_share_marker, name="delete_share_marker"),
]
