# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class ArServerConfig(AppConfig):
    name = 'AR_server'
